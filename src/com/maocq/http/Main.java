package com.maocq.http;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.google.gson.Gson;
import com.maocq.http.classes.Post;

public class Main {
	
	public final static Gson gson = new Gson();
	public static final String url = "http://jsonplaceholder.typicode.com/posts/1";

	public static void main(String[] args) {

	    String source = null;
	    
	    double inicio = System.currentTimeMillis();
	    
	    HttpClient httpClient = HttpClients.createDefault();
	    HttpGet httpGet = new HttpGet(url);
	    try {
	    	HttpResponse httpResponse = httpClient.execute(httpGet);
	        source = EntityUtils.toString(httpResponse.getEntity());
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	    
	    double fin = System.currentTimeMillis();
	    System.out.println(fin - inicio);
	    
	    //System.out.println(source);
		// Properties properties = gson.fromJson(source, Properties.class);
		// properties.getProperty("body")
	    
	    Post post = gson.fromJson(source, Post.class);
	    System.out.println(post.toString());
	    
	    generarJson();
	}
	
	public static void generarJson(){
		Post myPost = new Post(1,2,"Titulo", "Lorem ipsum");
		 String representacionJSON = gson.toJson(myPost);
		 System.out.println(representacionJSON);
	}

}
